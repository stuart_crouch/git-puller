# GIT PULLER #

This is a simple set of bash scripts that will recursively search a given path for git repositories and pull them.  It will write logs locally for the repository, and globally as an archive of when it was run

## Crontab

the `crontab` file should be pasted into a crontab or other batch runner, and the paths exchanged for the relevant locations.

## The scripts

The application itself is made up of a bash shell to search out all git repositories, and a bash shell to pull that repository and record the results.

    repo-update-search.sh

The search script will spawn a copy of the pull script on a new thread, and record it in the log.  Each thread records any logs it creates to a log directory at the same level as the .git folder.

    repo-update.sh

The pull script assumes that the account it is running under already has the required access (ssh keys, username/password, smoke signals + hand gestures) configured to allow a successful pull of the code.  It also assumes the local git repository is already pointing at the correct branch. It will issue a git pull command and then exit.
