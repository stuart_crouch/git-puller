#!/bin/bash

ROOT_DIR=$1

if [[ ! -n "$ROOT_DIR" ]]; then
  ROOT_DIR=$PWD
fi

echo "looking for things to do in ${ROOT_DIR}"

for dir in $(find ${ROOT_DIR} -name ".git")
do
  echo "spawning thread for ${dir}"
  if [[ ! -e ${dir}/../.logs ]]; then
    mkdir -p ${dir}/../.logs
  fi
  ( /usr/bin/repo-update.sh ${dir} >> ${dir}/../.logs/repo-update.log & )
done
