#!/bin/bash

dir=$1

if [[ ! -n "$dir" ]]; then
 echo "root directory required"
fi

cd "${dir}/.."

echo "========= starting $(date) ========="
git pull
echo "========= finished $(date) ========="
